
.onLoad <- function(libname, pkgname) {
  # CRAN OMP THREAD LIMIT
  Sys.setenv("OMP_THREAD_LIMIT" = 2)

}


if (getRversion() >= "2.15.1") {                                                

  vars <- c('.', 'label', 'id', 'Patient', 'Month', 'Parent_Code', 'V1', 'V2',
            'V3', 'code1', 'code2', 'value', 'db_name', 'clusters', 'Class ID',
            'Synonyms', 'CUI', 'name', 'label_synonyms', 'cui_names', 'parent',
            'Preferred Label', 'section', 'root', 'file_name', 'word', 'text')

  utils::globalVariables(vars)
}
