
from rocker/shiny-verse:4.4.2
run apt-get update && apt-get install -y texlive texlive-latex-recommended texlive-fonts-extra qpdf

run R -e "install.packages(c('data.table', 'Matrix', 'RcppAlgos', 'reshape2', 'rsvd', 'RSQLite', 'flexdashboard'))"

run apt-get update && apt-get install -y tidy

add ./ /nlpembeds
#run cd /nlpembeds && make roxygenise
run R -e "devtools::install('nlpembeds')"
